-----------------------------------------------------------------------------
                          MaltParser 1.9.2                             
-----------------------------------------------------------------------------
         MALT (Models and Algorithms for Language Technology) Group          
             Vaxjo University and Uppsala University                         
                             Sweden                                          
-----------------------------------------------------------------------------

Started: Sun Jun 17 21:21:50 CEST 2018
  Transition system    : Non-Projective
  Parser configuration : Covington with allow_root=true and allow_shift=false
  Data Format          : /nopproj-en-baseline-covnonproj-liblinear/conllu.xml
.          	      1	      0s	     44MB
.          	     10	      0s	     53MB
.          	    100	      1s	     91MB
..........	   1000	      3s	    136MB
..........	   2000	      4s	    337MB
           	   2002	      4s	    337MB
Parsing time: 00:00:04 (4702 ms)
Finished: Sun Jun 17 21:21:55 CEST 2018
-----------------------------------------------------------------------------
                          MaltParser 1.9.2                             
-----------------------------------------------------------------------------
         MALT (Models and Algorithms for Language Technology) Group          
             Vaxjo University and Uppsala University                         
                             Sweden                                          
-----------------------------------------------------------------------------

Started: Sun Jun 17 21:21:55 CEST 2018
  Transition system    : Non-Projective
  Parser configuration : Stack
  Data Format          : /nopproj-en-baseline-stackeager-liblinear/conllu.xml
.          	      1	      0s	     41MB
.          	     10	      0s	     43MB
.          	    100	      0s	     70MB
..........	   1000	      1s	     40MB
..........	   2000	      1s	     42MB
           	   2002	      1s	     45MB
Parsing time: 00:00:01 (1874 ms)
Finished: Sun Jun 17 21:21:57 CEST 2018
-----------------------------------------------------------------------------
                          MaltParser 1.9.2                             
-----------------------------------------------------------------------------
         MALT (Models and Algorithms for Language Technology) Group          
             Vaxjo University and Uppsala University                         
                             Sweden                                          
-----------------------------------------------------------------------------

Started: Sun Jun 17 21:21:57 CEST 2018
  Transition system    : Non-Projective
  Parser configuration : Stack
  Data Format          : /nopproj-en-baseline-stacklazy-liblinear/conllu.xml
.          	      1	      0s	     40MB
.          	     10	      0s	     42MB
.          	    100	      0s	     69MB
..........	   1000	      1s	     38MB
..........	   2000	      1s	     41MB
           	   2002	      1s	     41MB
Parsing time: 00:00:01 (1878 ms)
Finished: Sun Jun 17 21:21:59 CEST 2018
