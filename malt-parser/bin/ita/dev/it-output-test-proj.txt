-----------------------------------------------------------------------------
                          MaltParser 1.9.2                             
-----------------------------------------------------------------------------
         MALT (Models and Algorithms for Language Technology) Group          
             Vaxjo University and Uppsala University                         
                             Sweden                                          
-----------------------------------------------------------------------------

Started: Sun Jun 17 22:48:12 CEST 2018
  Transition system    : Projective
  Parser configuration : Stack
  Data Format          : /pproj-it-baseline-stackproj-liblinear/conllu.xml
.          	      1	      0s	     42MB
.          	     10	      0s	     43MB
.          	    100	      0s	     67MB
....       	    482	      1s	     56MB
Parsing time: 00:00:01 (1085 ms)
Finished: Sun Jun 17 22:48:13 CEST 2018
-----------------------------------------------------------------------------
                          MaltParser 1.9.2                             
-----------------------------------------------------------------------------
         MALT (Models and Algorithms for Language Technology) Group          
             Vaxjo University and Uppsala University                         
                             Sweden                                          
-----------------------------------------------------------------------------

Started: Sun Jun 17 22:48:13 CEST 2018
  Transition system    : Arc-Standard
  Parser configuration : Nivre with allow_root=true, allow_reduce=false and enforce_tree=false
  Data Format          : /pproj-it-baseline-nivrestandard-liblinear/conllu.xml
.          	      1	      0s	     42MB
.          	     10	      0s	     44MB
.          	    100	      0s	     67MB
....       	    482	      1s	     56MB
Parsing time: 00:00:01 (1110 ms)
Finished: Sun Jun 17 22:48:14 CEST 2018
