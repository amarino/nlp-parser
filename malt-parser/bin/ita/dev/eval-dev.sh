source config

export EVALUATION_FILES_PATH="../../../evalpl_comparison/"

# evaluation files
export OVERALL=$EVALUATION_FILES_PATH"01_overall.xml"
export CPOSTAG_ACCURACY=$EVALUATION_FILES_PATH"02_cpostag_accuracy.xml"
export CPOSTAG_ERROR_RATE=$EVALUATION_FILES_PATH"03_cpostag_error_rate.xml"
export DEPREL=$EVALUATION_FILES_PATH"04_deprel.xml"
export DEPREL_AND_ATTACHMENT=$EVALUATION_FILES_PATH"05_deprel_and_attachment.xml"
export HEAD_DIRECTION=$EVALUATION_FILES_PATH"06_head_direction.xml"
export HEAD_DISTANCE=$EVALUATION_FILES_PATH"07_head_distance.xml"
export FRAME_CONFUSION=$EVALUATION_FILES_PATH"08_frame_confusion.xml"
export FOCUS_WORD_ERRORS=$EVALUATION_FILES_PATH"09_focus_word_errors.xml"
export SENTENCE_ERRORS=$EVALUATION_FILES_PATH"10_sentence_errors.xml"

declare -a output_dev_ds_proj=('pproj-it-baseline-covproj-liblinear-dev-parsed' 'pproj-it-baseline-nivreeager-liblinear-dev-parsed' 'pproj-it-baseline-nivrestandard-liblinear-dev-parsed' 'pproj-it-baseline-stackproj-liblinear-dev-parsed')
declare -a output_dev_ds_noproj=('nopproj-it-baseline-covnonproj-liblinear-dev-parsed' 'nopproj-it-baseline-stackeager-liblinear-dev-parsed' 'nopproj-it-baseline-stacklazy-liblinear-dev-parsed')

declare -a arr=($OVERALL $CPOSTAG_ACCURACY $CPOSTAG_ERROR_RATE $DEPREL $DEPREL_AND_ATTACHMENT $HEAD_DIRECTION $HEAD_DISTANCE $FRAME_CONFUSION $FOCUS_WORD_ERRORS $SENTENCE_ERRORS)

for i in "${arr[@]}"	
do	
	out=$(echo "$i" | awk -F "/" '{print $NF}' | awk -F "." '{print $1}')
	echo $out
	java $JAVA_OPTS -jar $MALT_EVAL_PATH -e $i -s ${output_dev_ds_proj[@]} -g ${PROJECTIVIZED_DATASET_NAME} > ${out}_PROJ_IT_$dev_ds
	
	#java $JAVA_OPTS -jar $LIB_PATH$MALT_EVAL -e $i  -s $OUTPUT_PROJECTIVIZATION_MODEL_EN -g $ENGLISH_DEV_DS > $(echo ${out}_PROJ_EN)
	#java -jar $JAVA_OPTS $LIB_PATH$MALT_EVAL -e $i  -s $OUTPUT_NONPROJ_MODEL_EN -g $ENGLISH_DEV_DS > $(echo ${out}_NONPROJ_EN)
done

for i in "${arr[@]}"
do	
	out=$(echo "$i" | awk -F "/" '{print $NF}' | awk -F "." '{print $1}')
	java $JAVA_OPTS -jar $MALT_EVAL_PATH -e $i -s ${output_dev_ds_noproj[@]} -g $IT_DEV_DS > ${out}_NOPROJ_IT_$dev_ds
	
	#java $JAVA_OPTS -jar $LIB_PATH$MALT_EVAL -e $i  -s $OUTPUT_PROJECTIVIZATION_MODEL_EN -g $ENGLISH_DEV_DS > $(echo ${out}_PROJ_EN)
	#java -jar $JAVA_OPTS $LIB_PATH$MALT_EVAL -e $i  -s $OUTPUT_NONPROJ_MODEL_EN -g $ENGLISH_DEV_DS > $(echo ${out}_NONPROJ_EN)
done
