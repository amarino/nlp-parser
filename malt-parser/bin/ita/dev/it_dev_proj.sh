#!/bin/bash
set -e

source config

rm -f it-output-dev-proj.txt

##Projectivize BASELINE
java ${JAVA_OPTS} -jar ${MALT_PARSER_PATH} -c ${IT_OUTPUT_PROJECTIVIZATION_MODEL} -m proj -i ${IT_DEV_DS} -o ${PROJECTIVIZED_DATASET_NAME} -pp ${PROJECTIVIZATION_TYPE} -if ${DS_FORMAT}

declare -a ORACLES=('liblinear') #Da aggiungere perchè ci vuole tempo
declare -a PROJECTIVE_ALGOS=('nivreeager' 'nivrestandard' 'stackproj' 'covproj');
declare -A development_times

for algorithm in "${PROJECTIVE_ALGOS[@]}"
do
	for oracle in "${ORACLES[@]}"
	do
		echo "Testing with DEVELOPMENT_DATASET with algorithm: $algorithm and oracle: $oracle"
		java ${JAVA_OPTS} -jar ${MALT_PARSER_PATH} -c ${IT_OUTPUT_PROJECTIVIZATION_MODEL}-${PROJECTIVIZATION_TYPE}-${algorithm}-${oracle} -i ${PROJECTIVIZED_DATASET_NAME} -m parse -if ${DS_FORMAT} -o ${IT_OUTPUT_PROJECTIVIZATION_MODEL}-${PROJECTIVIZATION_TYPE}-${algorithm}-${oracle}-dev-parsed >> it-output-dev-proj.txt 2>&1

		result_milliseconds=$(cat it-output-dev-proj.txt | tail -n 2 | head -n 1 | awk '{ print substr($4,2) }')
		
		development_times[$algorithm-$oracle]=$result_milliseconds
	done
done

echo "Testing (DEV) times" 
for key in "${!development_times[@]}"; do
	echo $key - "${development_times[$key]} ms"; 
done
