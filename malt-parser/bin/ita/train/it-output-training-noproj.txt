-----------------------------------------------------------------------------
                          MaltParser 1.9.2                             
-----------------------------------------------------------------------------
         MALT (Models and Algorithms for Language Technology) Group          
             Vaxjo University and Uppsala University                         
                             Sweden                                          
-----------------------------------------------------------------------------

Started: Sun Jun 17 16:08:56 CEST 2018
  Transition system    : Non-Projective
  Parser configuration : Covington with allow_root=true and allow_shift=false
  Oracle               : Covington
  Data Format          : file:////home/eugenio/Documents/nlp-parser/malt-parser/bin/ita/train/nopproj-it-baseline-covnonproj-liblinear/conllu.xml
.          	      1	      0s	      5MB
.          	     10	      0s	      9MB
.          	    100	      0s	      6MB
..........	   1000	      2s	    184MB
..........	   2000	      4s	    231MB
..........	   3000	      6s	     47MB
..........	   4000	      8s	    129MB
..........	   5000	     10s	    104MB
..........	   6000	     13s	     32MB
..........	   7000	     16s	     42MB
..........	   8000	     18s	     93MB
..........	   9000	     18s	     60MB
..........	  10000	     20s	     75MB
..........	  11000	     23s	     28MB
..........	  12000	     25s	     32MB
..........	  13000	     26s	    177MB
.          	  13121	     26s	     62MB
Creating Liblinear model odm0.liblinear.moo
- Read all training instances.
- Train a parser model using LibLinear.
- Optimize the memory usage
- Save the Liblinear model odm0.liblinear.moo
Learning time: 00:02:52 (172065 ms)
Finished: Sun Jun 17 16:11:48 CEST 2018
-----------------------------------------------------------------------------
                          MaltParser 1.9.2                             
-----------------------------------------------------------------------------
         MALT (Models and Algorithms for Language Technology) Group          
             Vaxjo University and Uppsala University                         
                             Sweden                                          
-----------------------------------------------------------------------------

Started: Sun Jun 17 16:11:48 CEST 2018
  Transition system    : Non-Projective
  Parser configuration : Stack
  Oracle               : Swap-Eager
  Data Format          : file:////home/eugenio/Documents/nlp-parser/malt-parser/bin/ita/train/nopproj-it-baseline-stackeager-liblinear/conllu.xml
.          	      1	      0s	      5MB
.          	     10	      0s	      6MB
.          	    100	      0s	     22MB
..........	   1000	      0s	     46MB
..........	   2000	      1s	     18MB
..........	   3000	      2s	     42MB
..........	   4000	      2s	    205MB
..........	   5000	      3s	    136MB
..........	   6000	      4s	     59MB
..........	   7000	      4s	     33MB
..........	   8000	      5s	    178MB
..........	   9000	      5s	    253MB
..........	  10000	      5s	    148MB
..........	  11000	      6s	    112MB
..........	  12000	      6s	     68MB
..........	  13000	      7s	    215MB
.          	  13121	      7s	    233MB
Creating Liblinear model odm0.liblinear.moo
- Read all training instances.
- Train a parser model using LibLinear.
- Optimize the memory usage
- Save the Liblinear model odm0.liblinear.moo
Learning time: 00:00:41 (41567 ms)
Finished: Sun Jun 17 16:12:30 CEST 2018
-----------------------------------------------------------------------------
                          MaltParser 1.9.2                             
-----------------------------------------------------------------------------
         MALT (Models and Algorithms for Language Technology) Group          
             Vaxjo University and Uppsala University                         
                             Sweden                                          
-----------------------------------------------------------------------------

Started: Sun Jun 17 16:12:30 CEST 2018
  Transition system    : Non-Projective
  Parser configuration : Stack
  Oracle               : Swap-Lazy
  Data Format          : file:////home/eugenio/Documents/nlp-parser/malt-parser/bin/ita/train/nopproj-it-baseline-stacklazy-liblinear/conllu.xml
.          	      1	      0s	      5MB
.          	     10	      0s	      6MB
.          	    100	      0s	     22MB
..........	   1000	      0s	     45MB
..........	   2000	      1s	     15MB
..........	   3000	      2s	     39MB
..........	   4000	      2s	     64MB
..........	   5000	      3s	    259MB
..........	   6000	      3s	    194MB
..........	   7000	      4s	    173MB
..........	   8000	      4s	     74MB
..........	   9000	      4s	    154MB
..........	  10000	      5s	     68MB
..........	  11000	      5s	     45MB
..........	  12000	      6s	     19MB
..........	  13000	      6s	    169MB
.          	  13121	      6s	    186MB
Creating Liblinear model odm0.liblinear.moo
- Read all training instances.
- Train a parser model using LibLinear.
- Optimize the memory usage
- Save the Liblinear model odm0.liblinear.moo
Learning time: 00:00:40 (40114 ms)
Finished: Sun Jun 17 16:13:10 CEST 2018
