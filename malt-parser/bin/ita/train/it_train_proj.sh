#!/bin/bash
set -e

source config

rm -f it-output-training-proj.txt

##Projectivize BASELINE
java ${JAVA_OPTS} -jar ${MALT_PARSER_PATH} -c ${IT_OUTPUT_PROJECTIVIZATION_MODEL} -m proj -i ${IT_TRAIN_DS} -o ${PROJECTIVIZED_DATASET_NAME} -pp ${PROJECTIVIZATION_TYPE} -if ${DS_FORMAT}

declare -a ORACLES=('liblinear') #Da aggiungere perchè ci vuole tempo
declare -a PROJECTIVE_ALGOS=('nivreeager' 'nivrestandard' 'stackproj' 'covproj');
declare -A training_times

for algorithm in "${PROJECTIVE_ALGOS[@]}"
do
	for oracle in "${ORACLES[@]}"
	do
		#timestamp_start=$(date +%s)
		echo "(PROJ)Training with algorithm: $algorithm and oracle: $oracle"
		java ${JAVA_OPTS} -jar ${MALT_PARSER_PATH} -c ${IT_OUTPUT_PROJECTIVIZATION_MODEL}-${PROJECTIVIZATION_TYPE}-${algorithm}-${oracle} -i ${PROJECTIVIZED_DATASET_NAME} -m learn -if ${DS_FORMAT} -a ${algorithm} -l ${oracle} >> it-output-training-proj.txt 2>&1

		result_milliseconds=$(cat it-output-training-proj.txt | tail -n 2 | head -n 1 | awk '{ print substr($4,2) }')
		#timestamp_finish=$(date +%s)

		#training_times[$algorithm-$oracle]=$(expr $timestamp_finish - $timestamp_start)
		training_times[$algorithm-$oracle]=$result_milliseconds
	done
done

echo "Training times" 
for key in "${!training_times[@]}"; do
	echo "$key - ${training_times[$key]} ms";
done
