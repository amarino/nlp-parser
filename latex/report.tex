\documentclass[
  oneside,
  12pt, a4paper,
  footinclude=true,
  headinclude=true,
  cleardoublepage=empty
]{scrreprt}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[italian]{babel}
\usepackage{lipsum}
\usepackage{graphicx}
\usepackage[table]{xcolor}
\usepackage[linedheaders,parts,pdfspacing]{classicthesis}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{acronym}
\usepackage{listings}
\usepackage{fancyvrb}
\usepackage{array}
\usepackage{makecell}
\usepackage{eurosym}
\usepackage{multirow}
\usepackage{diagbox}
\usepackage{comment}
\lstset{
    language=Prolog,
    basicstyle=\scriptsize,
    numbers=left,
    stepnumber=1,
    showstringspaces=false,
    tabsize=1,
    breaklines=true,
    breakatwhitespace=false,
}
\usepackage{tikz}
\usetikzlibrary{arrows,positioning} 
\tikzset{
    %Define standard arrow tip
    >=stealth',
    %Define style for boxes
    punkt/.style={
           rectangle,
           rounded corners,
           draw=black, very thick,
           text width=6.5em,
           minimum height=2em,
           text centered},
    % Define arrow style
    pil/.style={
           ->,
           thick,
           shorten <=2pt,
           shorten >=2pt,}
}

\title{MaltParser e MEMM: analisi delle performance}
\subtitle{Corso di Tecniche di Comprensione di Linguaggio Naturale}
\author{Alessandro Marino, Eugenio Liso}
\date{\today}

\begin{document}

\maketitle

\tableofcontents

\chapter*{Introduzione}

\begin{comment}
Inserire descrizione algoritmi di parsing e oracolo liblinear
Commentare il problema riscontrato con oracolo libsvm
Introduzione varie sezioni
Commentare risultati
Estrarre 3 frasi da entrambi i testset e confrontare con i risultati del sito
\end{comment}

Questo report presenta i risultati derivanti dai test effettuati su due strumenti, il MaltParser ed il PoS tagger basato su Maximum Entropy Markov Models, che risolvono alcuni dei task più importanti nell'ambito del processamento del linguaggio naturale:  il parsing sintattico e il Part of speech tagging. Il primo è un sistema per il parsing a dipendenze "data-driven": dato in input un insieme di frasi vi associa una struttura sintattica. Il secondo strumento risolve il problema del Part-of-speech tagging, ovvero di associare, alle singole parole che compongono una frase, un TAG che rappresenta una parte del discorso (tale strumento sfrutta i principi dei modelli di Markov a massima entropia).

Ad una breve introduzione delle componenti che i due sistemi offrono e del problema in esame, seguirà la presentazione dei risultati ottenuti all'interno dei diversi scenari.

\chapter{MaltParser: un sistema per parsing a dipendenze data-driven}

In questo capitolo si introduce il MaltParser, gli algoritmi di parsing utilizzati all'interno dei test e i risultati ottenuti.

Gli step di \emph{training}, \emph{tuning} e \emph{testing} sono stati effettuati attraverso diversi dataset in formato \emph{conllu} per due lingue diverse: l'italiano e l'inglese.

Il codice è consultabile liberamente all'indirizzo: \url{https://gitlab.com/kadoodev/nlp-parser.git}

I dataset utilizzati per testare i vari modelli sono:

\begin{itemize}
	\item Italiano: \url{https://github.com/UniversalDependencies/UD_Italian-ISDT/tree/master}.
	\item Inglese: \url{https://github.com/UniversalDependencies/UD_English-EWT/tree/master}
\end{itemize}

\section{Algoritmi di Parsing e Oracolo}

Gli algoritmi di parsing utilizzati all'interno dei test sono:

\begin{itemize}
    \item \emph{Nivre Standard}
    \item \emph{Nivre Eager}
    \item \emph{Covington Proiettivo}
    \item \emph{Stack Proiettivo}
\end{itemize}
per quanto riguarda le strutture proiettive, mentre:
\begin{itemize}
    \item \emph{Stack Eager}
    \item \emph{Covington Non Proiettivo}
    \item \emph{Stack Lazy}
\end{itemize}
per le strutture non proiettive.

Il MaltParser permette inoltre la scelta tra due differenti tipologie di learner (oracoli):

\begin{itemize}
    \item \textbf{Liblinear:} libreria di machine learning per classificatori lineari
    \item \textbf{Libsvm:} libreria di machine learning per support vector machine
\end{itemize}

\textbf{A causa dell'impossibilità di portare a termine l'esecuzione con libsvm, i test includono esclusivamente i risultati ottenuti attraverso l'utilizzo del learner liblinear.}

\section{Analisi di Performance degli Algoritmi di Parsing}

In questa sezione si presentano i risultati, in termini di tempo di esecuzione, derivanti dalla fase di training per l'Italiano e per l'Inglese, per strutture proiettive e non proiettive.

\subsection{Fase di Training per l'Italiano}

\begin{center}
\rowcolors{2}{gray!25}{white}
\begin{tabular}{|c|c|}
\rowcolor{gray!50}
\hline
{\textbf{Projective Parser}} & {\emph{Oracolo}}\\
\emph{Algorithm} & \emph{liblinear (sec)}\\
\hline
nivreeager & $45.09$\\
\textbf{nivrestandard} & $40.23$\\
stackproj & $42.93$\\
covproj & $52.52$\\
\hline
\end{tabular}
\captionof{table}{Confronto dei tempi di esecuzione nella fase di training (projective parser) per l'Italiano}
\end{center}

\begin{center}
\rowcolors{2}{gray!25}{white}
\begin{tabular}{|c|c|}
\rowcolor{gray!50}
\hline
{\textbf{Non-Projective Parser}} & {\emph{Oracolo}}\\
\emph{Algorithm} & \emph{liblinear (sec)}\\
\hline
stackeager & $41.57$\\
\textbf{stacklazy} & $40.11$\\
covnonproj & $172.06$\\
\hline
\end{tabular}
\captionof{table}{Confronto dei tempi di esecuzione nella fase di training (non-projective parser) per l'Italiano}
\end{center}

Gli algoritmi più performanti in termini di tempo di esecuzione nella fase di training per l'italiano risultano essere:

\begin{itemize}
    \item \textbf{Nivre Standard} per i parser proiettivi
    \item \textbf{Stack Lazy} per i parser non proiettivi
\end{itemize}

\subsection{Fase di Training per l'Inglese}

\begin{center}
\rowcolors{2}{gray!25}{white}
\begin{tabular}{|c|c|}
\rowcolor{gray!50}
\hline
{\textbf{Projective Parser}} & {\emph{Oracolo}}\\
\emph{Algorithm} & \emph{liblinear (sec)}\\
\hline
nivreeager & $31.54$\\
\textbf{nivrestandard} & $28.40$\\
stackproj & $32.26$\\
covproj & $39.80$\\
\hline
\end{tabular}
\captionof{table}{Confronto dei tempi di esecuzione nella fase di training (projective parser) per l'Inglese}
\end{center}

\begin{center}
\rowcolors{2}{gray!25}{white}
\begin{tabular}{|c|c|}
\rowcolor{gray!50}
\hline
{\textbf{Non-Projective Parser}} & {\emph{Oracolo}}\\
\emph{Algorithm} & \emph{liblinear} (sec)\\
\hline
\textbf{stackeager} & $30.55$\\
stacklazy & $32.64$\\
covnonproj & $126.94$\\
\hline
\end{tabular}
\captionof{table}{Confronto dei tempi di esecuzione nella fase di training (non-projective parser) per l'Inglese}
\end{center}

Gli algoritmi più performanti in termini di tempo di esecuzione nella fase di training per la lingua Inglese risultano essere:

\begin{itemize}
    \item \textbf{Nivre Standard} per i parser proiettivi
    \item \textbf{Stack eager} per i parser non proiettivi
\end{itemize}

\subsection{Tempi di esecuzione sul Development Set per l'Italiano}

\begin{center}
\rowcolors{2}{gray!25}{white}
\begin{tabular}{|c|c|}
\rowcolor{gray!50}
\hline
{\textbf{Projective Parser}} & {\emph{Oracolo}}\\
\emph{Algorithm} & \emph{liblinear (sec)}\\
\hline
nivreeager & $1.16$\\
nivrestandard & $1.12$\\
stackproj & $1.13$\\
covproj & $1.31$\\
\hline
\end{tabular}
\captionof{table}{Confronto dei tempi di esecuzione sul development set (projective parser) per l'Italiano}
\end{center}
\newpage
\begin{center}
\rowcolors{2}{gray!25}{white}
\begin{tabular}{|c|c|}
\rowcolor{gray!50}
\hline
{\textbf{Non-Projective Parser}} & {\emph{Oracolo}}\\

\emph{Algorithm} & \emph{liblinear} (sec)\\
\hline
stackeager & $1.09$\\
stacklazy & $1.07$\\
covnonproj & $2.81$\\
\hline
\end{tabular}
\captionof{table}{Confronto dei tempi di esecuzione sul development set (non-projective parser) per l'Italiano}
\end{center}

\subsection{Tempi di esecuzione sul Development Set per l'Inglese}

\begin{center}
\rowcolors{2}{gray!25}{white}
\begin{tabular}{|c|c|}
\rowcolor{gray!50}
\hline
{\textbf{Projective Parser}} & {\emph{Oracolo}}\\

\emph{Algorithm} & \emph{liblinear (sec)}\\
\hline
nivreeager & $1.82$\\
nivrestandard & $1.72$\\
stackproj & $1.78$\\
covproj & $2.08$\\
\hline
\end{tabular}
\captionof{table}{Confronto dei tempi di esecuzione sul development set (projective parser) per l'Inglese}
\end{center}

\begin{center}
\rowcolors{2}{gray!25}{white}
\begin{tabular}{|c|c|}
\rowcolor{gray!50}
\hline
{\textbf{Non-Projective Parser}} & {\emph{Oracolo}}\\
\emph{Algorithm} & \emph{liblinear} (sec)\\
\hline
stackeager & $1.71$\\
stacklazy & $1.71$\\
covnonproj & $4.37$\\
\hline
\end{tabular}
\captionof{table}{Confronto dei tempi di esecuzione sul development set (non-projective parser) per l'Inglese}
\end{center}

\section{MaltEval: valutazione delle performance}

In questa sezione si introduce MaltEval, uno strumento utilizzato per la valutazione dei parser a dipendenze. MaltEval supporta diversi formati di dato ed offre la possibilità di estrarre diverse statistiche dall'output restituito dal parser. \textbf{In questa relazione si è deciso di porre particolare attenzione all'accuratezza del modello addestrato}, esplicitata attraverso tre diverse metriche:

\begin{itemize}
    \item \textbf{LA} (Labeled Attachment): rappresenta il numero di etichette di dipendenza uguali tra testset e goldset. 
    \item \textbf{UAS} (Unlabeled Attachment Score): rappresenta il numero di head uguali tra testset e goldset. 
    \item \textbf{LAS} (Labeled Attachment Score): rappresenta la metrica di default ed è il numero di volte nelle quali sia testa che l'etichetta di dipendenza assumono lo stesso valore nel testset e nel goldset.
\end{itemize}

Questa parte rappresenta la fase di tuning, all'interno della quale si valuta la scelta dei migliori algoritmi (in termini di accuratezza) da adottare nella fase finale di testing. Come anticipato in precedenza non è stato possibile effettuare confronti sulle performance dell'oracolo.

\subsection{Selezione degli algoritmi}

Di seguito le statistiche relative alle performance in termini di accuratezza dei diversi parser, con la successiva scelta dei $3$ scenari migliori.

\begin{center}
\rowcolors{2}{gray!25}{white}
\begin{tabular}{|c|c|c|c|}
\rowcolor{gray!50}
\hline
{\textbf{Projective Parser}}&\multicolumn{3}{c|}{\emph{Accuracy}}\\
\emph{Algorithm} & Metric: LA & Metric: UAS & Metric: LAS \\
\hline
nivreeager & $91.12\%$ &$87.71\%$ &$84.21\%$\\
\textbf{nivrestandard} & $91.94\%$ &$88.39\%$ &$85.06\%$\\
\textbf{stackproj} & $91.88\%$ & $88.39\%$ &$85.04\%$\\
covproj & $91.05\%$ & $86.56\%$ & $83.27\%$\\
\hline
\end{tabular}
\captionof{table}{Confronto accuratezza parsing proiettivo per l'Italiano sul development set}
\end{center}

\begin{center}
\rowcolors{2}{gray!25}{white}
\begin{tabular}{|c|c|c|c|}
\rowcolor{gray!50}
\hline
{\textbf{Non-Projective Parser}}&\multicolumn{3}{c|}{\emph{Accuracy}}\\
\emph{Algorithm} & Metric: LA & Metric: UAS & Metric: LAS \\
\hline
stackeager & $91.85\%$ &$88.07\%$ &$84.81\%$\\
\textbf{stacklazy} & $91.90\%$ &$88.08\%$ &$84.93\%$\\
covnonproj & $91.03\%$ &$86.57\%$ &$83.33\%$\\
\hline
\end{tabular}
\captionof{table}{Confronto accuratezza parsing non-proiettivo per l'Italiano sul development set}
\end{center}

Di seguito i $3$ i tre algoritmi più performanti in termini di accuratezza per l'italiano:

\begin{itemize}
    \item \textbf{Parsing Proiettivo:} nivre standard, stack proiettivo
    \item \textbf{Parsing Non-Proiettivo:} stack lazy
\end{itemize}

\begin{center}
\rowcolors{2}{gray!25}{white}
\begin{tabular}{|c|c|c|c|}
\rowcolor{gray!50}
\hline
{\textbf{Projective Parser}}&\multicolumn{3}{c|}{\emph{Accuracy}}\\
\emph{Algorithm} & Metric: LA & Metric: UAS & Metric: LAS \\
\hline
nivreeager & $89.35\%$ &$85.14\%$ &$82.13\%$\\
nivrestandard & $89.72\%$ &$85.07\%$ &$82.41\%$\\
\textbf{stackproj} & $89.77\%$ &$85.13\%$ &$82.48\%$\\
covproj & $89.01\%$ & $84.30\%$ & $81.61\%$\\
\hline
\end{tabular}
\captionof{table}{Confronto accuratezza parsing proiettivo per l'Inglese sul development set}
\end{center}

\begin{center}
\rowcolors{2}{gray!25}{white}
\begin{tabular}{|c|c|c|c|}
\rowcolor{gray!50}
\hline
{\textbf{Non-Projective Parser}}&\multicolumn{3}{c|}{\emph{Accuracy}}\\

\emph{Algorithm} & Metric: LA & Metric: UAS & Metric: LAS \\
\hline
\textbf{stackeager} & $89.78\%$ &$85.21\%$ &$82.50\%$\\
\textbf{stacklazy} & $89.79\%$ &$85.21\%$ &$82.54\%$\\
covnonproj & $89.07\%$ &$84.42\%$ &$81.75\%$\\
\hline
\end{tabular}
\captionof{table}{Confronto accuratezza parsing non-proiettivo per l'Inglese sul development set}
\end{center}

Di seguito i $3$ i tre algoritmi più performanti in termini di accuratezza per l'inglese:

\begin{itemize}
    \item \textbf{Parsing Proiettivo:} stack proiettivo
    \item \textbf{Parsing Non-Proiettivo:} stack eager, stack lazy
\end{itemize}

\newpage

\section{Risultati: Testing, visualizzazione e confronto degli alberi di parsing}

In questa sezione si mostrano accuratezza e tempi di esecuzione dei migliori $3$ algoritmi, per l'italiano e per l'inglese, a confronto.

\begin{center}
\rowcolors{2}{gray!25}{white}
\begin{tabular}{|c|c|c|c|c|c|}
\rowcolor{gray!50}
\hline
\emph{Algorithm} & LA & UAS & LAS & \emph{Metric Mean} & \emph{Runtime (sec)} \\
\hline
stacklazy & $91.33\%$ &$88.22\%$ &$84.78\%$\ & $88.11\%$ & $11.43$ \\
nivrestandard & $91.36\%$ &$88.23\%$ &$84.70\%$ & $88.10\%$ & $10.85$ \\
\textbf{stackproj} & $91.32\%$ & $88.31\%$ &$84.75\%$ & $88.13\%$ & $11.58$ \\
\hline
\end{tabular}
\captionof{table}{Confronto accuracy sul testing set, parser proiettivo per l'Italiano}
\end{center}

\begin{center}
\rowcolors{2}{gray!25}{white}
\begin{tabular}{|c|c|c|c|c|c|}
\rowcolor{gray!50}
\hline
\emph{Algorithm} & LA & UAS & LAS & \emph{Metric Mean} & \emph{Runtime (sec)} \\
\hline
stacklazy & $89.22\%$ &$84.71\%$ &$81.93\%$ & $85.29\%$ & $18.95$\\
stackeager & $89.09\%$ &$84.42\%$ &$81.67\%$ & $85.06\%$ & $19.35$\\
\textbf{stackproj} & $89.32\%$ & $85.14\%$ &  $82.16\%$ & $85.54\%$ & $18.51$ \\
\hline
\end{tabular}
\captionof{table}{Confronto accuracy sul testing set, parser proiettivo per l'Inglese}
\end{center}

L'algoritmo di parsing che presenta la migliore accuratezza risulta essere lo Stack Proiettivo, tanto per lo scenario in lingua Italiana (accuratezza media di $88.13\%$) quanto per quello in lingua Inglese (accuratezza media di $85.54\%$).

Le immagini che seguono rappresentano gli alberi di parsing, ottenuti attraverso lo strumento MaltEval, relativi a:

\begin{itemize}
\item Dataset Gold
\item Risultato del parsing tramite MaltParser
\item Risultato del parsing tramite lo strumento UDPipe
\end{itemize}

I test sono stati effettuati considerando $3$ frasi estratte dal dataset di test. I risultati mostrano le differenze nei risultati ottenuti dal MaltParser e da UDPipe.

\newpage

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.2]{img/it_gold_1.png}
\caption{Albero di parsing, dataset gold italiano per la frase "GOTEBORG - È stata la giornata di il doppio oro italiano ai Modiali di atletica:"}\label{fig:it_gold_1}
\end{center}
\end{figure}

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.2]{img/it_parsed_1.png}
\caption{MaltParser: Albero di parsing, dataset parsed test con algoritmo stack proiettivo italiano per la frase "GOTEBORG - È stata la giornata di il doppio oro italiano ai Modiali di atletica:"}\label{fig:it_parsed_1}
\end{center}
\end{figure}

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.2]{img/it_parsed_udpipe_1.png}
\caption{UDPipe: Albero di parsing, dataset parsed test italiano per la frase "GOTEBORG - È stata la giornata di il doppio oro italiano ai Modiali di atletica:"}\label{fig:it_parsed_udpipe_1}
\end{center}
\end{figure}

\newpage

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.2]{img/it_gold_2.png}
\caption{Albero di parsing, dataset gold italiano per la frase "Lo storico, paternalistico disinteresse di la chiesa cattolica verso il loro "genio" è a la fine."}\label{fig:it_gold_1}
\end{center}
\end{figure}

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.2]{img/it_parsed_2.png}
\caption{MaltParser: Albero di parsing, dataset parsed test con algoritmo stack proiettivo italiano per la frase "Lo storico, paternalistico disinteresse di la chiesa cattolica verso il loro "genio" è a la fine."}\label{fig:it_parsed_2}
\end{center}
\end{figure}

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.2]{img/it_parsed_udpipe_2.png}
\caption{UDPipe: Albero di parsing, dataset parsed test italiano per la frase "Lo storico, paternalistico disinteresse di la chiesa cattolica verso il loro "genio" è a la fine."}\label{fig:it_parsed_udpipe_2}
\end{center}
\end{figure}

\newpage

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.2]{img/it_gold_3.png}
\caption{Albero di parsing, dataset gold italiano per la frase "La sperimentazione di l'atomica ha cambiato il mondo per sempre, si disse subito dopo Hiroshima."}\label{fig:it_gold_1}
\end{center}
\end{figure}

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.2]{img/it_parsed_3.png}
\caption{MaltParser: Albero di parsing, dataset parsed test con algoritmo stack proiettivo italiano per la frase "La sperimentazione di l'atomica ha cambiato il mondo per sempre, si disse subito dopo Hiroshima."}\label{fig:it_parsed_3}
\end{center}
\end{figure}

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.2]{img/it_parsed_udpipe_3.png}
\caption{UDPipe: Albero di parsing, dataset parsed test italiano per la frase "La sperimentazione di l'atomica ha cambiato il mondo per sempre, si disse subito dopo Hiroshima."}\label{fig:it_parsed_udpipe_3}
\end{center}
\end{figure}

\newpage

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.2]{img/en_gold_1.png}
\caption{Albero di parsing, dataset gold inglese per la frase "What if Google expanded on its search-engine (and now e-mail) wares into a full-flaged operating system?"}\label{fig:en_gold_1}
\end{center}
\end{figure}

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.2]{img/en_parsed_1.png}
\caption{MaltParser: Albero di parsing, dataset parsed test con algoritmo proiettivo inglese per la frase "What if Google expanded on its search-engine (and now e-mail) wares into a full-flaged operating system?"}\label{fig:en_parsed_1}
\end{center}
\end{figure}

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.2]{img/en_parsed_udpipe_1.png}
\caption{UDPipe: Albero di parsing, dataset parsed test inglese per la frase "What if Google expanded on its search-engine (and now e-mail) wares into a full-flaged operating system?"}\label{fig:en_parsed_udpipe_1}
\end{center}
\end{figure}

\newpage

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.2]{img/en_gold_2.png}
\caption{Albero di parsing, dataset gold inglese per la frase "John Donovan from Argghhh! has put out a excellent slide show on what was actually found and fought for inn Fallujah"}\label{fig:en_gold_1}
\end{center}
\end{figure}

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.2]{img/en_parsed_2.png}
\caption{MaltParser: Albero di parsing, dataset parsed test con algoritmo proiettivo inglese per la frase "John Donovan from Argghhh! has put out a excellent slide show on what was actually found and fought for inn Fallujah"}\label{fig:en_parsed_2.png}
\end{center}
\end{figure}

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.2]{img/en_parsed_udpipe_2.png}
\caption{UDPipe: Albero di parsing, dataset parsed test inglese per la frase "John Donovan from Argghhh! has put out a excellent slide show on what was actually found and fought for inn Fallujah"}\label{fig:en_parsed_udpipe_2.png}
\end{center}
\end{figure}

\newpage

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.2]{img/en_gold_3.png}
\caption{Albero di parsing, dataset gold inglese per la frase "The are certainly being nasty to the United Nations Security Council in connection with the anti-proliferation treaty."}\label{fig:en_gold_3}
\end{center}
\end{figure}

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.2]{img/en_parsed_3.png}
\caption{MaltParser: Albero di parsing, dataset parsed test con stack proiettivo inglese per la frase "The are certainly being nasty to the United Nations Security Council in connection with the anti-proliferation treaty."}\label{fig:en_parsed_3}
\end{center}
\end{figure}

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.2]{img/en_parsed_udpipe_3.png}
\caption{UDPipe: Albero di parsing, dataset parsed test inglese per la frase "The are certainly being nasty to the United Nations Security Council in connection with the anti-proliferation treaty."}\label{fig:en_parsed_udpipe_3}
\end{center}
\end{figure}

\newpage

\subsection{Visualizzazione risultati del parsing attraverso UDPipe}

UDPipe è uno strumento utile per la risoluzione di task di tokenizzazione, tagging, lemmatizzazione e parsing a dipendenze di file conllu. In precedenza è stato utilizzato per generare l'output del parsing in formato conllu, in questa sezione si presenta una rappresentazione alternativa degli alberi di parsing (relativa alle stesse frasi estratte dal testset) ottenuta direttamente dallo strumento in questione.

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.8]{img/it_UDpipe_1.png}
\caption{Albero di parsing risultante dall'utilizzo di UDPipe, frase 1 Italiano}\label{fig:it_UDpipe_1}
\end{center}
\end{figure}

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.6]{img/it_UDpipe_2.png}
\caption{Albero di parsing risultante dall'utilizzo di UDPipe, frase 2 Italiano}\label{fig:it_UDpipe_1}
\end{center}
\end{figure}

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.8]{img/it_UDpipe_3.png}
\caption{Albero di parsing risultante dall'utilizzo di UDPipe, frase 3 Italiano}\label{fig:it_UDpipe_1}
\end{center}
\end{figure}

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.6]{img/en_UDpipe_1.png}
\caption{Albero di parsing risultante dall'utilizzo di UDPipe, frase 1 inglese}\label{fig:it_UDpipe_1}
\end{center}
\end{figure}

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.8]{img/en_UDpipe_2.png}
\caption{Albero di parsing risultante dall'utilizzo di UDPipe, frase 2 inglese}\label{fig:it_UDpipe_1}
\end{center}
\end{figure}

\begin{figure}[!hbtp]
\begin{center}
\includegraphics[scale=0.8]{img/en_UDpipe_3.png}
\caption{Albero di parsing risultante dall'utilizzo di UDPipe, frase 3 inglese}\label{fig:it_UDpipe_1}
\end{center}
\end{figure}

\newpage
\section{Conclusioni finali}

In definitiva, dai risultati ottenuti mediante il confronto fra il miglior algoritmo derivato dai test sul MaltParser (Stack Proiettivo) e lo strumento UDPipe, \textbf{è possibile affermare che il MaltParser ottiene in generale performance migliori rispetto a UDPipe, sia per quanto riguarda l'italiano che l'inglese}.

\chapter{Part-of-Speech tagging basato su Maximum Entropy Markov Models}


\section{MEMM Tagger}

Il MEMM Tagger è un particolare tipo di POS Tagger stocastico che utilizza un modello discriminativo per poter predire il tag del part-of-speech. 

Il sostanziale vantaggio di questo approccio è rappresentato dalla facilità con cui è possibile aggiungere nuove features al nostro modello.

Il modello consiste in due parti:

\begin{itemize}
	\item Un modello n-grammi (aggregato con feature custom) per le sequenze dei POS
	\item Un modello per la distribuzione della verosimiglianza dei POS per le parole
\end{itemize}

Queste parti sono combinate utilizzando il \emph{Teorema di Bayes}, e l'algoritmo di \emph{Viterbi} è utilizzato per trovare i POS più probabili data una sequenza di parole.

Il codice sorgente può essere visionato al seguente link: \url{https://gitlab.com/Warrior1992/MemmParser.git}

\subsection{Parametri}

Di seguito vengono presentati i vari parametri applicabili al MEMM Tagger.

\subsubsection{Algoritmo}

Sono disponibili due algoritmi che consentono di scegliere il POS più probabile:

\begin{itemize}
	\item Algoritmo di Viterbi
	\item Algoritmo Greedy
\end{itemize}

\subsubsection{Iper-Parametri}

Gli iper-parametri del MEMM Tagger sono:

\begin{itemize}
	\item regolarizzazione \emph{L2}, utilizzata per la regressione logistica
	\item il numero minimo di occorrenze nel dataset di training per poter utilizzare quella feature
\end{itemize}

\subsubsection{Features}

\paragraph{Features di Default}

Sono presenti molteplici features nel nostro modello del MEMM Parser. Per default, sono utilizzate le seguenti features:

\begin{itemize}
	
	\item UNIGRAM$_W$: La parola $w_t$ stessa
	\item PREVIOUS\_TAG\_POS: Il POS tag assegnato alla parola precedente $w_{t-1}$
	\item PREFIX$_W$: Il prefisso della parola $w_t$, definito con i primi 3 caratteri della parola
	\item SUFFIX$_W$: Il suffisso della parola $w_t$, definito con gli ultimi due caratteri della parola
	\item BIGRAM$_{W1,W2}$:
	\begin{itemize}
		\item Condizione sul passato: Il primo tipo di bigramma è la parola $w_1=w_t$ condizionata sulla parola precedente $w_2=w_{t-1}$, ovvero $P(w_1 = w_t | w_2 = w_{t-1})$
		\item Condizione sul futuro: Il secondo tipo di bigramma è la parola $w_1=w_t$ condizionata sulla parola successiva $w_2=w_{t+1}$, ovvero $P(w_1 = w_t | w_2 = w_{t+1})$
	\end{itemize}

	\item TRIGRAM$_{W1,W2,W3}$:
	\begin{itemize}
		\item Il trigramma è rappresentato da $P(w_2 = w_t | w_1=w_{t-1} \wedge w_3 = w_{t+1})$
		\item Si è scelto di utilizzare la finestra $w_{t-1}, w_t, w_{t+1}$ in quanto produce contiene informazioni sul passato e sul futuro, data la parola corrente $w_t$	
	\end{itemize}
	\item FIRST\_WORD\_IN\_SEQUENCE: True se la parola $w_t$ è la prima parola nella sequenza
	\item LAST\_WORD\_IN\_SEQUENCE: True se la parola $w_t$ è l'ultima parola nella sequenza
	\item NUMERIC: True se la parola $w_t$ è un numero, con possibile inclusione di punteggiatura e caratteri (ad esempio \$10,224 oppure 1954\euro)
	\item FIRST\_UPPER: True se il primo carattere della parola $w_t$ è maiuscolo

\end{itemize}

\paragraph{Features adattate per l'italiano}

Sono state aggiunte le seguenti features, adattate per l'italiano:

\begin{itemize}
	\item PREFIX\_NOMINAL\_ADJ\_ITALIAN: True se la parola $w_t$ contiene uno dei seguenti prefissi nominali o aggettivali\footnote{I prefissi sono tratti da \url{http://www.treccani.it/enciclopedia/prefissi\_\%28La-grammatica-italiana\%29/}} (utili per capire se la parola $w_t$ è un nome o un aggettivo):
	a, ante, con, circum, circon, contra, contro, per, anti, iper, ipo, sin, meta
	
	\item PREFIX\_VERB\_ITALIAN: True se la parola $w_t$ contiene un prefisso verbale. I prefissi verbali utilizzati sono: inter, tra, fra, contra, contro, de, dis, re, ri, stra
	
	\item WORD\_WITH\_HYPHEN: True se la parola $w_t$ contiene un trattino
	
	\item UPPER\_WORD: True se la parola $w_t$ è completamente maiuscola
	
	\item DIGIT\_AND\_DASH: True se la parola $w_t$ è un numero e contiene un trattino
	
	\item FOLLOWED\_BY\_INITIALS: True se la parola $w_t$ è maiuscola ed è seguita da una delle seguenti parole: Co. , Inc. , etc. 
\end{itemize}

\section{Training}

L'addestramento ed il testing dei modelli sono stati eseguiti utilizzando il dataset Universal Dependency Treebank per l'italiano, ed in particolare si è scelto un dataset ottenuto dalla conversione dall' Italian Stanford Dependency Treebank (ISDT). Tale dataset è disponibile su 

\url{https://github.com/UniversalDependencies/UD_Italian-ISDT/tree/master}.

Sul precedente dataset si sono eliminati spazi ridondanti e commenti.

Nella tabella \ref{training-MEMM} è possibile visualizzare i tempi di training del MEMM Parser.

\begin{table}[h]
	\begin{center} 
		\rowcolors{2}{gray!25}{white} 
		\begin{tabular}{|c|c|c|c|c|} 
			\rowcolor{gray!50} 
			\hline 
			\theadfont\diagbox[width=12em]{\\L2 Reg}{\#MinFeatOccurence}
			&1&2&3&5\\ 
			\hline 
			$0.8$ & $65,306$ & $51,616$ & $48,862$ & $44,344$\\ 
			$0.85$ & $67,884$ & $53,366$ & $48,085$ & $44,497$\\ 
			$0.9$ & $71,827$ & $52,641$ & $49,820$ & $44,818$\\ 
			$0.95$ & $70,364$ & $53,611$ & $51,261$ & $44,815$\\ 
			$1$ & $70,994$ & $54,104$ & $53,208$ & $49,507$\\ 
			\hline 
		\end{tabular}
		\caption{Training Time del MEMM Parser} 
		\label{training-MEMM}
	\end{center}
\end{table}

\section{Tuning con Development Set}

Dopo aver effettuato il training del MEMM Parser, si è proceduto al tuning del modello con il Development Set dell'UD Treebank dell'italiano. Il test è stato effettuato utilizzando un diverso algoritmo (Viterbi o greedy) e diverse features (custom o meno).

\subsection{Senza custom features}

\begin{table}[!hbtp]
	\begin{center} 
		\rowcolors{2}{gray!25}{white} 
		\begin{tabular}{|c|c|c|c|c|} 
			\rowcolor{gray!50} 
			\hline 
			\theadfont\diagbox[width=12em]{\\L2 Reg}{\#MinFeatOccurence}
			&1&2&3&5\\ 
			\hline 
			$0.8$ & $0.946$ & $0.948$ & $0.946$ & $0.943$\\ 
			$0.85$ & $0.946$ & $0.949$ & $0.946$ & $0.944$\\ 
			$0.9$ & $0.947$ & $0.949$ & $0.947$ & $0.944$\\ 
			$0.95$ & $0.947$ & $0.950$ & $0.948$ & $0.944$\\ 
			$1$ & $0.947$ & $0.951$ & $0.948$ & $0.945$\\ 
			\hline 
		\end{tabular} 
		\caption{Accuracy del MEMM Parser sul Development Set (Algoritmo di Viterbi senza custom features)} 
		\label{dev-MEMM-viterbi-nocust}
	\end{center}
\end{table}

\begin{table}[!hbtp]
	\begin{center} 
		\rowcolors{2}{gray!25}{white} 
		\begin{tabular}{|c|c|c|c|c|} 
			\rowcolor{gray!50} 
			\hline 
			\theadfont\diagbox[width=12em]{\\L2 Reg}{\#MinFeatOccurence}
			&1&2&3&5\\ 
			\hline 
			$0.8$ & $0.943$ & $0.944$ & $0.943$ & $0.942$\\ 
			$0.85$ & $0.944$ & $0.946$ & $0.944$ & $0.942$\\ 
			$0.9$ & $0.945$ & $0.946$ & $0.945$ & $0.943$\\ 
			$0.95$ & $0.946$ & $0.946$ & $0.946$ & $0.943$\\ 
			$1$ & $0.946$ & $0.947$ & $0.947$ & $0.943$\\ 
			\hline 
		\end{tabular} 
		\caption{Accuracy del MEMM Parser sul Development Set (Algoritmo greedy senza custom features)} 
		\label{dev-MEMM-greedy-nocust}
	\end{center}
\end{table}

\subsection{Con features custom (adattate per l'italiano)}

Di seguito i risultati con i vari algoritmi utilizzando features adattate per l'italiano.

\begin{table}[!hbtp]
	\begin{center} 
		\rowcolors{2}{gray!25}{white} 
		\begin{tabular}{|c|c|c|c|c|} 
			\rowcolor{gray!50} 
			\hline 
			\theadfont\diagbox[width=12em]{\\L2 Reg}{\#MinFeatOccurence}
			&1&2&3&5\\ 
			\hline 
			$0.8$ & $0.955$ & $0.956$ & $0.955$ & $0.950$\\ 
			$0.85$ & $0.955$ & $0.956$ & $0.955$ & $0.950$\\ 
			$0.9$ & $0.956$ & $0.957$ & $0.955$ & $0.950$\\ 
			$0.95$ & $0.956$ & \textcolor{red}{$0.957$} & $0.955$ & $0.950$\\ 
			$1$ & $0.956$ & \textcolor{red}{$0.957$} & $0.955$ & $0.950$\\ 
			\hline 
		\end{tabular} 
		\caption{Accuracy del MEMM Parser sul Development Set (Algoritmo di Viterbi con custom features)} 
		\label{dev-MEMM-viterbi-cust}
	\end{center}
\end{table}

\begin{table}[!hbtp]
	\begin{center} 
		\rowcolors{2}{gray!25}{white} 
		\begin{tabular}{|c|c|c|c|c|} 
			\rowcolor{gray!50} 
			\hline 
			\theadfont\diagbox[width=12em]{\\L2 Reg}{\#MinFeatOccurence}
			&1&2&3&5\\ 
			\hline 
			$0.8$ & $0.954$ & $0.953$ & $0.952$ & $0.946$\\ 
			$0.85$ & $0.954$ & $0.954$ & $0.952$ & $0.946$\\ 
			$0.9$ & $0.954$ & $0.955$ & $0.952$ & $0.947$\\ 
			$0.95$ & $0.955$ & $0.955$ & $0.952$ & $0.947$\\ 
			$1$ & $0.955$ & \textcolor{red}{$0.955$} & $0.953$ & $0.948$\\ 
			\hline 
		\end{tabular} 
		\caption{Accuracy del MEMM Parser sul Development Set (Algoritmo greedy con custom features)} 
		\label{dev-MEMM-greedy-cust}
	\end{center}
\end{table}

\subsection{Considerazioni Testing con Development Set}

Da una prima analisi dei risultati, è chiaro che l'aggiunta delle features adattate per l'italiano contribuisce al miglioramento delle performance del parser. Risulta inoltre che l'algoritmo di Viterbi si comporta meglio di quello greedy, e sommariamente i migliori risultati si ottengono utilizzando un valore di L2 vicino ad 1 ed un numero di occorrenze minime della feature pari a 2.

\section{Testing con Test Set}

Dalla fase di testing precedente con il Development Set, sono stati selezionati i 3 modelli più promettenti. Di seguito i risultati

\begin{center}
	\hspace*{-2.5cm}\begin{tabular}{ | l | c | c | c |}
		\hline
		Algoritmo & L2 & \#MinFeatureOccurrence & Accuracy \\
		\hline
		MEMM - Viterbi - custom features & 0.95 & 2 & 0.955 (9490.0/9935.0) \\ \hline
		MEMM - Viterbi - custom features & 1 & 2 & \textcolor{red}{0.955 (9492.0/9935.0)} \\ \hline
		MEMM - greedy - custom features & 1 & 2 & 0.953 (9473.0/9935.0) \\
		\hline
	\end{tabular}
\end{center}

Il miglior algoritmo risulta essere MEMM con algoritmo di Viterbi e custom features, L2 = 1 e \#MinFeatOccurrence = 2.

\end{document}